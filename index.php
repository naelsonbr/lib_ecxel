<?php
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;

//require __DIR__ . '/../Header.php';

$inputFileType = 'Xls';
$inputFileName = 'planilha.xls';

$reader = IOFactory::createReader($inputFileType);
$reader->setReadDataOnly(true);
$spreadsheet = $reader->load($inputFileName);

$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

echo "<pre>";
var_dump($sheetData);
echo "</pre>";
